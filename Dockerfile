FROM php:7.3-cli-stretch

RUN apt-get update && apt-get install -y \
        apt-utils \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev \
        unzip \
        zlib1g-dev \
        libbz2-dev \
        libzip-dev \
        mysql-client \
    && docker-php-ext-install -j$(nproc) iconv \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install -j$(nproc) gd \
    && docker-php-ext-install pdo pdo_mysql zip

# Install Composer and make it available in the PATH
ENV COMPOSER_ALLOW_SUPERUSER 1

# install composer
RUN curl -o /tmp/composer-setup.php https://getcomposer.org/installer \
    && curl -o /tmp/composer-setup.sig https://composer.github.io/installer.sig \
    && php -r "if (hash('SHA384',file_get_contents('/tmp/composer-setup.php'))!==trim(file_get_contents('/tmp/composer-setup.sig'))){unlink('/tmp/composer-setup.php');echo 'Invalid installer'.PHP_EOL; exit(1);}" \
    && php /tmp/composer-setup.php \
        --no-ansi \
        --install-dir=/usr/local/bin \
        --filename=composer \
        --snapshot

RUN composer --version

COPY .docker/php/php-ini-overrides.ini /usr/local/etc/php/conf.d/99-overrides.ini

ENV APP_DIR /var/www/html
ENV APPLICATION_ENV development

WORKDIR $APP_DIR
EXPOSE 80
#VOLUME $APP_DIR
CMD ["php", "-S", "0.0.0.0:80", "-t", "./public"]