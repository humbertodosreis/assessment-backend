<?php


namespace App\Domain;

/**
 * Interface ProductRepository
 * @package App\Domain
 */
interface ProductRepository
{
    /**
     * @return Product[]
     */
    public function all(): array;

    /**
     * @param Product $product
     */
    public function add(Product $product): void;

    /**
     * @param Product $product
     */
    public function update(Product $product): void;

    /**
     * @param int $id
     */
    public function remove(int $id): void;

    /**
     * @param int $id
     * @return Product|null
     */
    public function find(int $id): ?Product;

    /**
     * @return int
     */
    public function count(): int;

    /**
     * @param int $limit
     * @return Product[]
     */
    public function listRandomly(int $limit = 4): array;

    /**
     * @param string $sku
     * @return Product|null
     */
    public function findBySku(string $sku): ?Product;
}