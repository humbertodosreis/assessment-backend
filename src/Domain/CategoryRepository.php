<?php declare(strict_types=1);


namespace App\Domain;


interface CategoryRepository
{
    /**
     * @return array
     */
    public function all(): array;

    /**
     * @param Category $category
     */
    public function add(Category $category): void;

    /**
     * @param Category $category
     */
    public function update(Category $category): void;

    /**
     * @param int $id
     */
    public function remove(int $id): void;

    /**
     * @param int $id
     * @return Category|null
     */
    public function find(int $id): ?Category;

    /**
     * @param string $code
     * @return Category|null
     */
    public function findByCode(string $code): ?Category;
}