<?php declare(strict_types=1);


namespace App\Domain;

use Assert\Assertion;

/**
 * Class Category
 * @package App\Domain
 */
class Category
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $code;

    /**
     * @param array $data
     * @return Category
     */
    static public function fromArray(array $data): Category
    {
        $category = new self();

        if (isset($data['id'])) {
            $category->setId((int)$data['id']);
        }

        $category->setName($data['name'])
            ->setCode($data['code']);

        return $category;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return (int)$this->id;
    }

    /**
     * @param int $id
     * @return Category
     */
    public function setId(int $id): Category
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Category
     */
    public function setName(string $name): Category
    {
        Assertion::notEmpty($name, 'Name is required');

        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return Category
     */
    public function setCode(string $code): Category
    {
        Assertion::notEmpty($code, 'Code is required');

        $this->code = $code;
        return $this;
    }
}