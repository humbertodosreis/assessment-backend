<?php


namespace App\Foundation;

use Assert\Assertion;
use Generator;

class CsvReader
{
    /**
     * @var string
     */
    private $filename;
    /**
     * @var bool
     */
    private $ignoreFirstLine;

    /**
     * CsvReader constructor.
     * @param string $filename
     * @param bool $ignoreFirstLine
     */
    public function __construct(string $filename, bool $ignoreFirstLine = false)
    {

        Assertion::file($filename);

        $this->filename = $filename;
        $this->ignoreFirstLine = $ignoreFirstLine;
    }

    /**
     * @return Generator
     */
    public function read(): Generator
    {
        $handle = fopen($this->filename, 'r');
        $counter = 0;

        while (($data = fgetcsv($handle, 0, ";")) !== FALSE) {
            if ($this->ignoreFirstLine && $counter++ == 0) {
                continue;
            }

            yield $data;
        }

        fclose($handle);
    }
}