<?php declare(strict_types=1);


namespace App\Infra\Persistence\MySQL;

use App\Domain\Category;
use App\Domain\Product;
use App\Domain\ProductRepository;
use PDO;

/**
 * Class MySQLProductRepository
 * @package App\Infra\Persistence\MySQL
 */
class MySQLProductRepository implements ProductRepository
{

    /**
     * @var PDO
     */
    private $connection;

    /**
     * MySQLProductRepository constructor.
     * @param PDO $connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @return array
     */
    public function all(): array
    {
        $query = <<<SQL
        SELECT 
            p.id, p.name, p.sku, p.price, p.quantity, p.description,
            c.id category_id, c.name category_name, c.code category_code
        FROM products p
        LEFT JOIN products_x_categories ON product_id=p.id
        LEFT JOIN categories c ON c.id = category_id
        ORDER BY p.id
SQL;

        $rows = $this->connection->query($query)->fetchAll(PDO::FETCH_ASSOC);

        return $this->build($rows);
    }

    /**
     * @param Product $product
     */
    public function add(Product $product): void
    {
        $stmt = $this->connection->prepare('INSERT products(`sku`, `name`, `price`, `quantity`, `description`) VALUES (?, ?, ?, ?, ?)');
        $stmt->execute([
            $product->getSku(),
            $product->getName(),
            $product->getPrice(),
            $product->getQuantity(),
            $product->getDescription()
        ]);

        $productId = $this->connection->lastInsertId();
        $product->setId((int) $productId);

        foreach ($product->getCategories() as $category) {
            $this->connection->prepare('INSERT INTO products_x_categories(product_id, category_id) VALUES (? , ?)')
                ->execute([$productId, $category->getId()]);
        }
    }

    /**
     * @param Product $product
     */
    public function update(Product $product): void
    {
        $stmt = $this->connection->prepare('UPDATE products SET `sku`=?, `name`=?, `price`=?, `quantity`=?, `description`=? WHERE id=?');
        $stmt->execute([
            $product->getSku(),
            $product->getName(),
            $product->getPrice(),
            $product->getQuantity(),
            $product->getDescription(),
            $product->getId()
        ]);

        $delStmt = $this->connection->prepare('DELETE FROM products_x_categories WHERE product_id=?');
        $delStmt->execute([$product->getId()]);

        foreach ($product->getCategories() as $category) {
            $this->connection->prepare('INSERT INTO products_x_categories(product_id, category_id) VALUES (?, ?)')
                ->execute([$product->getId(), $category->getId()]);
        }
    }

    /**
     * @param int $id
     */
    public function remove(int $id): void
    {
        $this->connection->prepare('DELETE FROM products WHERE id=?')
            ->execute([$id]);
    }

    /**
     * @param int $id
     * @return Product|null
     */
    public function find(int $id): ?Product
    {
        $query = <<<SQL
        SELECT 
            p.id, p.name, p.sku, p.price, p.quantity, p.description,
            c.id category_id, c.name category_name, c.code category_code
        FROM products p
        LEFT JOIN products_x_categories ON product_id=p.id
        LEFT JOIN categories c ON c.id = category_id
        WHERE p.id=?
SQL;

        $stmt = $this->connection->prepare($query);
        $stmt->execute([$id]);

        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (empty($rows)) {
            return null;
        }

        return $this->build($rows)[0];
    }

    /**
     * @param array $rows
     * @return array
     */
    private function build(array $rows): array
    {
        if (empty($rows)) {
            return [];
        }

        $collection = $categories = [];
        $prev = [];
        $current = true;

        while ($current) {

            $current = array_shift($rows);

            if (isset($current['category_id'])) {
                $categories[] = Category::fromArray([
                    'id' => $current['category_id'],
                    'name' => $current['category_name'],
                    'code' => $current['category_code'],
                ]);
            }

            if (isset($prev['id']) && $prev['id'] != $current['id'] || empty($current)) {
                $last = !empty($current) && is_array($categories) && count($categories) > 1 ? array_pop($categories) : null;

                $collection[] = Product::fromArray([
                    'id' => $prev['id'],
                    'sku' => $prev['sku'],
                    'name' => $prev['name'],
                    'price' => $prev['price'],
                    'quantity' => $prev['quantity'],
                    'description' => $prev['description'],
                    'categories' => $categories
                ]);

                $categories = $last ? [$last] : [];
            }

            $prev = $current;
        }

        return $collection;
    }

    /**
     * @inheritDoc
     */
    public function count(): int
    {
        $row = $this->connection->query('SELECT COUNT(*) FROM products')
            ->fetch();

        return (int) $row[0];        $query = <<<SQL
        SELECT 
            p.id, p.name, p.sku, p.price, p.quantity, p.description,
            c.id category_id, c.name category_name, c.code category_code
        FROM products p
        LEFT JOIN products_x_categories ON product_id=p.id
        LEFT JOIN categories c ON c.id = category_id
        WHERE p.id=?
SQL;

        $stmt = $this->connection->prepare($query);
        $stmt->execute([$id]);

        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (empty($rows)) {
            return null;
        }

        return $this->build($rows)[0];
    }

    /**
     * @inheritDoc
     */
    public function listRandomly(int $limit = 4): array
    {
        $query = <<<SQL
        SELECT
            p.id, p.name, p.sku, p.price, p.quantity, p.description,
            c.id category_id, c.name category_name, c.code category_code
        FROM (SELECT * FROM products ORDER BY RAND() LIMIT ?) as p
        LEFT JOIN products_x_categories ON product_id=p.id
        LEFT JOIN categories c ON c.id=category_id
SQL;
        $stmt = $this->connection->prepare($query);
        $stmt->bindValue(1, $limit, PDO::PARAM_INT);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (empty($rows)) {
            return [];
        }

        return $this->build($rows);
    }

    /**
     * @inheritDoc
     */
    public function findBySku(string $sku): ?Product
    {
        $query = <<<SQL
        SELECT 
            p.id, p.name, p.sku, p.price, p.quantity, p.description,
            c.id category_id, c.name category_name, c.code category_code
        FROM products p
        LEFT JOIN products_x_categories ON product_id=p.id
        LEFT JOIN categories c ON c.id = category_id
        WHERE p.sku=?
SQL;

        $stmt = $this->connection->prepare($query);
        $stmt->execute([$sku]);

        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (empty($rows)) {
            return null;
        }

        return $this->build($rows)[0];
    }


}