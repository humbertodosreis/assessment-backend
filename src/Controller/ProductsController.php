<?php declare(strict_types=1);


namespace App\Controller;

use App\Domain\CategoryRepository;
use App\Domain\Product;
use App\Domain\ProductRepository;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use RuntimeException;
use Zend\Diactoros\Response\RedirectResponse;

/**
 * Class ProductsController
 * @package App
 */
class ProductsController extends BaseController
{

    /**
     * @var ProductRepository
     */
    private $productMapper;

    /**
     * @var CategoryRepository
     */
    private $categoryMapper;

    /**
     * @param ProductRepository $categoryMapper
     */
    public function setProductMapper(ProductRepository $categoryMapper)
    {
        $this->productMapper = $categoryMapper;
    }

    /**
     * @return ProductRepository
     */
    public function getProductMapper()
    {
        if (null === $this->productMapper) {
            throw new RuntimeException('Product mapper not defined');
        }

        return $this->productMapper;
    }

    /**
     * @return CategoryRepository
     */
    public function getCategoryMapper()
    {
        if (null === $this->categoryMapper) {
            throw new RuntimeException('Category mapper not defined');
        }

        return $this->categoryMapper;
    }

    /**
     * @param CategoryRepository $categoryMapper
     */
    public function setCategoryMapper(CategoryRepository $categoryMapper)
    {
        $this->categoryMapper = $categoryMapper;
    }

    /**
     * @return ResponseInterface
     */
    public function list()
    {
        $products = $this->getProductMapper()->all();

        return $this->render('products-list', ['products' => $products]);
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface|RedirectResponse
     */
    public function add(ServerRequestInterface $request)
    {
        $categories = $this->getCategoryMapper()->all();

        if ($request->getMethod() == 'POST') {
            $data = $request->getParsedBody();
            $selectedCategories = [];

            foreach ($data['categories'] as $value) {
                $selectedCategories[] = $this->getCategoryMapper()->find((int)$value);
            }

            $product = Product::fromArray([
                'sku' => $data['sku'],
                'name' => $data['name'],
                'price' => (float) $data['price'],
                'quantity' => (int) $data['quantity'],
                'description' => $data['description'],
                'categories' => $selectedCategories
            ]);

            $this->getProductMapper()->add($product);

            return new RedirectResponse('/products?added=1');
        }

        return $this->render('products-form', ['categories' => $categories, 'new' => true]);
    }

    public function edit(ServerRequestInterface $request)
    {
        $id = (int)$request->getAttribute('id');

        $categories = $this->getCategoryMapper()->all();
        $product = $this->getProductMapper()->find($id);

        if ($request->getMethod() == 'POST') {
            $data = $request->getParsedBody();

            $selectedCategories = [];

            foreach ($data['categories'] as $value) {
                $selectedCategories[] = $this->getCategoryMapper()->find((int)$value);
            }

            $product->setSku($data['sku'])
                ->setName($data['name'])
                ->setPrice((float)$data['price'])
                ->setQuantity((int)$data['quantity'])
                ->setDescription($data['description'])
                ->setCategories($selectedCategories);

            $this->getProductMapper()->update($product);

            return new RedirectResponse('/products?edited=1');
        }

        return $this->render('products-form', ['product' => $product, 'categories' => $categories, 'new' => false]);
    }

    public function remove(ServerRequestInterface $request)
    {
        $id = (int)$request->getAttribute('id');
        $product = $this->getProductMapper()->find($id);
        $this->getProductMapper()->remove($product->getId());

        return new RedirectResponse('/products?removed=1');
    }
}