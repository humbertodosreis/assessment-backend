# Solução

O teste foi implementado sem a utilização de um framework. 

As libs **Zend\Diactoros**, **FastRoute** e **Relay** foram utilizados para implementar o middleware web. 
A lib **Blade** foi utilizada para a separação da View do Controller (templates).

**Phinx** é a lib utilizada respoonsável por criar e gerenciar os migrates e para testes o **Codeception** foi a opção utilizada.

**PHP-DI** é a lib para ajudar na injeção de dependências, e a sua configuração esta no arquivo bootstrap.php.

O console do importador de produto utiliza a lib Symfony\Console. 

E por fim, o projeto roda no PHP 7.3 e MySQL 5.7 utilizando containers Docker com o auxílio do Docker Compose. 

### Executar
```
make install
make migration
```

### Rodar os testes
```
make run-tests
```

### Executar importador
```
make run-importer
```

O projeto irá rodar no link http://localhost:8090