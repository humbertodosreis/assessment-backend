<?php namespace Domain;

use App\Domain\Category;
use App\Domain\Product;

class ProductTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testNewProduct()
    {
        $data = [
            'id' => 1,
            'sku' => '1234-000',
            'name' => 'Product Name',
            'price' => 1000,
            'quantity' => 10,
            'categories' => [Category::fromArray(['id' => 1, 'name' => 'Category name', 'code' => 'Category code'])],
            'description' => 'Descriṕtion'
        ];

        $product = Product::fromArray($data);

        $this->assertEquals($data['id'], $product->getId());
        $this->assertEquals($data['sku'], $product->getSku());
        $this->assertEquals($data['name'], $product->getName());
        $this->assertEquals($data['price'], $product->getPrice());
        $this->assertEquals($data['quantity'], $product->getQuantity());
        $this->assertEquals($data['categories'], $product->getCategories());
        $this->assertEquals($data['description'], $product->getDescription());
    }

    public function testProductCanNotHaveEmptySku()
    {
        $this->expectException(\Assert\InvalidArgumentException::class);

        $data = [
            'id' => 1,
            'sku' => '',
            'name' => 'A name',
            'price' => 1000,
            'quantity' => 10,
            'categories' => [],
            'description' => 'Description'
        ];

        $product = Product::fromArray($data);
    }

    public function testProductCanNotHaveEmptyName()
    {
        $this->expectException(\Assert\InvalidArgumentException::class);

        $data = [
            'id' => 1,
            'sku' => '123456-000',
            'name' => '',
            'price' => 1000.00,
            'quantity' => 10,
            'categories' => [],
            'description' => 'Description'
        ];

        $product = Product::fromArray($data);
    }

    public function testPriceCanBeDecimal()
    {
        $this->expectException(\TypeError::class);

        $data = [
            'id' => 1,
            'sku' => '123456-000',
            'name' => 'A name',
            'price' => 'a price',
            'quantity' => 10,
            'categories' => [],
            'description' => 'Description'
        ];

        $product = Product::fromArray($data);
    }

    public function testQuantityCanBeInteger()
    {
        $this->expectException(\TypeError::class);

        $data = [
            'id' => 1,
            'sku' => '123456-000',
            'name' => 'A name',
            'price' => 1000.00,
            'quantity' => 'price is 10',
            'categories' => [],
            'description' => 'Description'
        ];

        $product = Product::fromArray($data);
    }

    public function testQuantityCanNotBeNegative()
    {
        $this->expectException(\Assert\InvalidArgumentException::class);

        $data = [
            'id' => 1,
            'sku' => '123456-000',
            'name' => 'A name',
            'price' => 1000.00,
            'quantity' => -1000,
            'categories' => [],
            'description' => 'Description'
        ];

        $product = Product::fromArray($data);
    }

    public function testCategoriesShouldBeAnArrayOfObjectsOfTheTypeCategory()
    {
        $this->expectException(\Assert\InvalidArgumentException::class);

        $data = [
            'id' => 1,
            'sku' => '123456-000',
            'name' => 'A name',
            'price' => 1000.00,
            'quantity' => -1000,
            'categories' => ['banana', 0],
            'description' => 'Description'
        ];

        $product = Product::fromArray($data);
    }
}