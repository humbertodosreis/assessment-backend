<?php namespace Infra\Persistence\MySQL;

use App\Domain\Category;
use App\Infra\Persistence\MySQL\MySQLCategoryRepository;
use League\FactoryMuffin\FactoryMuffin;
use PDO;

class MySQLCategoryRepositoryTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var FactoryMuffin
     */
    protected static $fm;
    
    protected function _before()
    {
        static::$fm = new FactoryMuffin();
        static::$fm->loadFactories(dirname(__DIR__, 5) . '/tests/_support/_factories');
    }

    /**
     * @return MySQLCategoryRepository
     */
    private function createCategoryRepository()
    {
        return new MySQLCategoryRepository(
            new PDO(
                'mysql:dbname=' . env('DBTEST_NAME')  . ';host=' . env('DBTEST_HOST'),
                env('DBTEST_USER'),
                env('DBTEST_PASSWORD'),
                [
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                    PDO::MYSQL_ATTR_COMPRESS => true,
                    PDO::MYSQL_ATTR_MULTI_STATEMENTS => true

                ]
            )
        );
    }

    // tests
    public function testItShouldCreateCategory()
    {
        $repository = $this->createCategoryRepository();

        $category = self::$fm->instance(Category::class);

        $repository->add($category);

        $saved = $repository->find($category->getId());

        $this->assertEquals($category->getId(), $saved->getId());
        $this->assertEquals($category->getName(), $saved->getName());
        $this->assertEquals($category->getCode(), $saved->getCode());
    }

    public function testItShouldFindById()
    {
        $repository = $this->createCategoryRepository();

        $category = self::$fm->instance(Category::class);

        $repository->add($category);

        $categoryFromRepository = $repository->find($category->getId());

        $this->assertInstanceOf(Category::class, $categoryFromRepository);
        $this->assertEquals($category->getId(), $categoryFromRepository->getId());
    }


    public function testItShouldFindByCode()
    {
        $repository = $this->createCategoryRepository();

        $category = self::$fm->instance(Category::class);

        $repository->add($category);

        $categoryFromRepository = $repository->findByCode($category->getCode());

        $this->assertInstanceOf(Category::class, $categoryFromRepository);
        $this->assertEquals($category->getCode(), $categoryFromRepository->getCode());
    }

    public function testItShouldUpdateCategory()
    {
        $repository = $this->createCategoryRepository();

        $category = self::$fm->instance(Category::class);

        $repository->add($category);

        $newName = 'Category updated';
        $newCode = '12345-333';

        $saved = $repository->find($category->getId());
        $saved->setName($newName);
        $saved->setCode($newCode);

        $repository->update($saved);

        $updated = $repository->find($category->getId());

        $this->assertEquals($newName, $updated->getName());
        $this->assertEquals($newCode, $updated->getCode());
    }

    public function testItShouldDeleteCategory()
    {
        $repository = $this->createCategoryRepository();

        $category = self::$fm->instance(Category::class);

        $repository->add($category);

        $repository->remove($category->getId());

        $categoryFromRepository = $repository->find($category->getId());

        $this->assertNull($categoryFromRepository);
    }
}