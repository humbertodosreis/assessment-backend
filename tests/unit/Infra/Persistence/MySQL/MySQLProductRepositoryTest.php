<?php namespace Infra\Persistence\MySQL;

use App\Domain\Category;
use App\Domain\Product;
use App\Infra\Persistence\MySQL\MySQLCategoryRepository;
use App\Infra\Persistence\MySQL\MySQLProductRepository;
use League\FactoryMuffin\FactoryMuffin;
use PDO;

class MySQLProductRepositoryTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var FactoryMuffin
     */
    protected static $fm;

    protected function _before()
    {
        static::$fm = new FactoryMuffin();
        static::$fm->loadFactories(dirname(__DIR__, 5) . '/tests/_support/_factories');
    }

    private function createConnection()
    {
        return new PDO(
            'mysql:dbname=' . env('DBTEST_NAME')  . ';host=' . env('DBTEST_HOST'),
            env('DBTEST_USER'),
            env('DBTEST_PASSWORD'),
            [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::MYSQL_ATTR_COMPRESS => true,
                PDO::MYSQL_ATTR_MULTI_STATEMENTS => true

            ]
        );
    }

    /**
     * @return MySQLCategoryRepository
     */
    private function createCategoryRepository()
    {
        return new MySQLCategoryRepository($this->createConnection());
    }

    /**
     * @return MySQLProductRepository
     */
    private function createProductRepository()
    {
        return new MySQLProductRepository($this->createConnection());
    }


    // tests
    public function testItShouldCreateProduct()
    {
        $categoryRepository = $this->createCategoryRepository();

        $categoryA = self::$fm->instance(Category::class);
        $categoryB = self::$fm->instance(Category::class);

        $categoryRepository->add($categoryA);
        $categoryRepository->add($categoryB);

        $productRepository = $this->createProductRepository();

        $categories = [$categoryA, $categoryB];

        /**
         * @var Product
         */
        $product = self::$fm->instance(Product::class);
        $product->setCategories($categories);

        $productRepository->add($product);

        $productFromRepository = $productRepository->find($product->getId());

        $this->assertEquals($product->getId(), $productFromRepository->getId());
        $this->assertEquals($product->getSku(), $productFromRepository->getSku());
        $this->assertEquals($product->getName(), $productFromRepository->getName());
        $this->assertEqualsWithDelta($product->getPrice(), $productFromRepository->getPrice(), 0.1);
        $this->assertEquals($product->getQuantity(), $productFromRepository->getQuantity());
        $this->assertEquals($product->getDescription(), $productFromRepository->getDescription());
        $this->assertCount(count($categories), $productFromRepository->getCategories());
    }

    public function testItShouldUpdateProduct()
    {
        $categoryRepository = $this->createCategoryRepository();

        $categoryA = self::$fm->instance(Category::class);
        $categoryB = self::$fm->instance(Category::class);
        $categoryC = self::$fm->instance(Category::class);

        $categoryRepository->add($categoryA);
        $categoryRepository->add($categoryB);
        $categoryRepository->add($categoryC);

        $productRepository = $this->createProductRepository();

        $categories = [$categoryA];

        /**
         * @var Product
         */
        $product = self::$fm->instance(Product::class);
        $product->setCategories($categories);

        $productRepository->add($product);

        $productFromRepository = $productRepository->find($product->getId());
        $categories = [$categoryA, $categoryC];

        $productFromRepository->setName('Name update');
        $productFromRepository->setDescription('Description updated');
        $productFromRepository->setCategories($categories);

        $productRepository->update($productFromRepository);

        $productUpdated = $productRepository->find($productFromRepository->getId());

        $this->assertEquals($productFromRepository->getName(), $productUpdated->getName());
        $this->assertEquals($productFromRepository->getDescription(), $productUpdated->getDescription());
        $this->assertCount(count($categories), $productUpdated->getCategories());
    }


}