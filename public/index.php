<?php
declare(strict_types=1);

use App\Controller\DashboardController;
use App\Controller\ProductsController;
use App\Controller\CategoriesController;
use FastRoute\RouteCollector;
use Middlewares\FastRoute;
use Middlewares\RequestHandler;
use Narrowspark\HttpEmitter\SapiEmitter;
use Relay\Relay;
use Zend\Diactoros\ServerRequestFactory;
use function FastRoute\simpleDispatcher;

require_once dirname(__DIR__) . '/bootstrap.php';

$routes = simpleDispatcher(function (RouteCollector $r) {
    $r->get('/', DashboardController::class);

    $r->addGroup('/products', function (RouteCollector $r) {
        $r->get('', [ProductsController::class, 'list']);
        $r->addRoute(['GET', 'POST'], '/add', [ProductsController::class, 'add']);
        $r->addRoute(['GET', 'POST'], '/{id:[0-9]+}/edit', [ProductsController::class, 'edit']);
        $r->addRoute('GET', '/{id:[0-9]+}/remove', [ProductsController::class, 'remove']);
    });

    // categories
    $r->addGroup('/categories', function (RouteCollector $r) {
        $r->get('', [CategoriesController::class, 'list']);
        $r->addRoute(['GET', 'POST'], '/add', [CategoriesController::class, 'add']);
        $r->addRoute(['GET', 'POST'], '/{id:[0-9]+}/edit', [CategoriesController::class, 'edit']);
        $r->addRoute('GET', '/{id:[0-9]+}/remove', [CategoriesController::class, 'remove']);
    });
});

$middlewareQueue[] = new FastRoute($routes);
$middlewareQueue[] = new RequestHandler($container);

/** @noinspection PhpUnhandledExceptionInspection */
$requestHandler = new Relay($middlewareQueue);
$response = $requestHandler->handle(ServerRequestFactory::fromGlobals());

$emitter = new SapiEmitter();
/** @noinspection PhpVoidFunctionResultUsedInspection */
return $emitter->emit($response);