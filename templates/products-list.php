<?php $this->layout('main') ?>
<?php $this->start('main-content') ?>
    <div class="header-list-page">
        <h1 class="title">Products</h1>
        <a href="/products/add" class="btn-action">Add new Product</a>
    </div>
    <table class="data-grid">
        <tr class="data-row">
            <th class="data-grid-th">
                <span class="data-grid-cell-content">Name</span>
            </th>
            <th class="data-grid-th">
                <span class="data-grid-cell-content">SKU</span>
            </th>
            <th class="data-grid-th">
                <span class="data-grid-cell-content">Price</span>
            </th>
            <th class="data-grid-th">
                <span class="data-grid-cell-content">Quantity</span>
            </th>
            <th class="data-grid-th">
                <span class="data-grid-cell-content">Categories</span>
            </th>

            <th class="data-grid-th">
                <span class="data-grid-cell-content">Actions</span>
            </th>
        </tr>
        <?php foreach ($products as $product): ?>
            <tr class="data-row">
                <td class="data-grid-td">
                    <span class="data-grid-cell-content"><?= $this->e($product->getName()) ?></span>
                </td>

                <td class="data-grid-td">
                    <span class="data-grid-cell-content"><?= $this->e($product->getSku()) ?></span>
                </td>

                <td class="data-grid-td">
                    <span class="data-grid-cell-content">R$ <?= $this->e($product->getPrice()) ?></span>
                </td>

                <td class="data-grid-td">
                    <span class="data-grid-cell-content"><?= $this->e($product->getQuantity()) ?></span>
                </td>

                <td class="data-grid-td">
                    <span class="data-grid-cell-content"><?= array_reduce($product->getCategories(), function ($carry, $item) { return $carry . $item->getName() . '<br>'; }, '')?></span>
                </td>

                <td class="data-grid-td">
                    <div class="actions">
                        <div class="action edit"><a href="/products/<?= $product->getId() ?>/edit">Edit</a></div>
                        <div class="action delete"><a href="/products/<?= $product->getId() ?>/remove">Delete</a></div>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
<?php $this->stop() ?>
