<?php $this->layout('main') ?>
<?php $this->start('main-content') ?>
<div class="header-list-page">
    <h1 class="title">Categories</h1>
    <a href="/categories/add" class="btn-action">Add new Category</a>
</div>
<table class="data-grid">
    <tr class="data-row">
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Name</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Code</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Actions</span>
        </th>
    </tr>
    <?php foreach ($categories as $category): ?>
        <tr class="data-row">
            <td class="data-grid-td">
                <span class="data-grid-cell-content"><?= $this->e($category->getName()) ?></span>
            </td>

            <td class="data-grid-td">
                <span class="data-grid-cell-content"><?= $this->e($category->getCode()) ?></span>
            </td>

            <td class="data-grid-td">
                <div class="actions">
                    <div class="action edit"><a href="/categories/<?= $category->getId() ?>/edit">Edit</a></div>
                    <div class="action delete"><a href="/categories/<?= $category->getId() ?>/remove">Delete</a></div>
                </div>
            </td>
        </tr>
    <?php endforeach; ?>
</table>
<?php $this->stop() ?>
