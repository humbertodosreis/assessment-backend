<?php $this->layout('main') ?>
<?php $this->start('main-content') ?>
<?php
$selectedCategories = [];

if (!$new) {
    $selectedCategories = array_map(function ($item) {
        return $item->getId();
    }, $product->getCategories());
}

?>

    <h1 class="title new-item"><?= $new ? 'New Product' : 'Edit Product "' . $product->getName() . '"' ?></h1>

    <form method="post"
          action="<?= $new ? '/products/add' : '/products/' . $product->getId() . '/edit' ?>"
          enctype="application/x-www-form-urlencoded">
        <div class="input-field">
            <label for="sku" class="label">Product SKU</label>
            <input type="text" id="sku" name="sku" class="input-text" value="<?= $new ? '' : $product->getSku() ?>"/>
        </div>
        <div class="input-field">
            <label for="name" class="label">Product Name</label>
            <input type="text" id="name" name="name" class="input-text" value="<?= $new ? '' : $product->getName() ?>"/>
        </div>
        <div class="input-field">
            <label for="price" class="label">Price</label>
            <input type="text" id="price" name="price" class="input-text"
                   value="<?= $new ? '' : $product->getPrice() ?>"/>
        </div>
        <div class="input-field">
            <label for="quantity" class="label">Quantity</label>
            <input type="text" id="quantity" name="quantity" class="input-text"
                   value="<?= $new ? '' : $product->getQuantity() ?>"/>
        </div>
        <div class="input-field">
            <label for="category" class="label">Categories</label>
            <select multiple id="category" name="categories[]" class="input-text">
                <?php foreach ($categories as $category): ?>
                    <option value="<?= $category->getId() ?>" <?= in_array($category->getId(), $selectedCategories) ? 'selected' : '' ?> ><?= $this->e($category->getName()) ?></option>
                <? endforeach; ?>
            </select>
        </div>
        <div class="input-field">
            <label for="description" class="label">Description</label>
            <textarea id="description" name="description"
                      class="input-text"><?= $new ? '' : $product->getDescription() ?></textarea>
        </div>
        <div class="actions-form">
            <a href="/products" class="action back">Back</a>
            <input class="btn-submit btn-action" type="submit" value="Save Product"/>
        </div>
    </form>
<?php $this->stop() ?>