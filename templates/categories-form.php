<?php $this->layout('main') ?>
<?php $this->start('main-content') ?>
    <h1 class="title new-item"><?= $new ? 'New Category' : 'Edit Category "' . $this->e($category->getName()) . '"' ?></h1>

<?php if ($new): ?>
    <form method="post" action="/categories/add" enctype="application/x-www-form-urlencoded">
        <div class="input-field">
            <label for="category-name" class="label">Category Name</label>
            <input type="text" id="category-name" name="name" class="input-text"/>

        </div>
        <div class="input-field">
            <label for="category-code" class="label">Category Code</label>
            <input type="text" id="category-code" name="code" class="input-text"/>

        </div>
        <div class="actions-form">
            <a href="/categories" class="action back">Back</a>
            <input class="btn-submit btn-action" type="submit" value="Save"/>
        </div>
    </form>
<?php else: ?>
    <form method="post" action="/categories/<?= $category->getId() ?>/edit" enctype="application/x-www-form-urlencoded">
        <div class="input-field">
            <label for="category-name" class="label">Category Name</label>
            <input type="text" id="category-name" name="name" value="<?= $category->getName() ?>" class="input-text"/>

        </div>
        <div class="input-field">
            <label for="category-code" class="label">Category Code</label>
            <input type="text" id="category-code" name="code" value="<?= $category->getCode() ?>" class="input-text"/>

        </div>
        <div class="actions-form">
            <a href="/categories" class="action back">Back</a>
            <input class="btn-submit btn-action" type="submit" value="Save"/>
        </div>
    </form>
<?php endif; ?>
<?php $this->stop() ?>